 # -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-TODAY Acespritech Solutions Pvt Ltd <http://www.acespritech.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.addons.product import _common
from openerp import SUPERUSER_ID

class stock_move(models.Model):
    _inherit = 'stock.move'

    def _action_explode(self, cr, uid, move, context=None):
        """ Explodes pickings.
        @param move: Stock moves
        @return: True
        """
        if context is None:
            context = {}
        bom_obj = self.pool.get('mrp.bom')
        move_obj = self.pool.get('stock.move')
        prod_obj = self.pool.get("product.product")
        proc_obj = self.pool.get("procurement.order")
        uom_obj = self.pool.get("product.uom")
        to_explode_again_ids = []
        property_ids = context.get('property_ids') or []
        bis = bom_obj._bom_find(cr, SUPERUSER_ID, product_id=move.product_id.id, properties=property_ids)
        bom_point = bom_obj.browse(cr, SUPERUSER_ID, bis, context=context)
        if bis and bom_point.type == 'phantom':
            
            processed_ids = []
            factor = uom_obj._compute_qty(cr, SUPERUSER_ID, move.product_uom.id, move.product_uom_qty, bom_point.product_uom.id) / bom_point.product_qty
            res = bom_obj._bom_explode(cr, SUPERUSER_ID, bom_point, move.product_id, factor, property_ids, context=context)

            for line in res[0]:
                product = prod_obj.browse(cr, uid, line['product_id'], context=context)
                if product.type != 'service':
                    valdef = {
                        'picking_id': move.picking_id.id if move.picking_id else False,
                        'product_id': line['product_id'],
                        'product_uom': line['product_uom'],
                        'product_uom_qty': line['product_qty'],
                        'product_uos': line['product_uos'],
                        'product_uos_qty': line['product_uos_qty'],
                        'state': 'draft',  #will be confirmed below
                        'name': line['name'],
                        'procurement_id': move.procurement_id.id,
                        'split_from': move.id, #Needed in order to keep sale connection, but will be removed by unlink
                    }
                    mid = move_obj.copy(cr, uid, move.id, default=valdef, context=context)
                    to_explode_again_ids.append(mid)
                else:
                    if prod_obj.need_procurement(cr, uid, [product.id], context=context):
                        valdef = {
                            'name': move.rule_id and move.rule_id.name or "/",
                            'origin': move.origin,
                            'company_id': move.company_id and move.company_id.id or False,
                            'date_planned': move.date,
                            'product_id': line['product_id'],
                            'product_qty': line['product_qty'],
                            'product_uom': line['product_uom'],
                            'product_uos_qty': line['product_uos_qty'],
                            'product_uos': line['product_uos'],
                            'group_id': move.group_id.id,
                            'priority': move.priority,
                            'partner_dest_id': move.partner_id.id,
                            }
                        if move.procurement_id:
                            proc = proc_obj.copy(cr, uid, move.procurement_id.id, default=valdef, context=context)
                        else:
                            proc = proc_obj.create(cr, uid, valdef, context=context)
                        proc_obj.run(cr, uid, [proc], context=context) #could be omitted
            
            #check if new moves needs to be exploded
            if to_explode_again_ids:
                for new_move in self.browse(cr, uid, to_explode_again_ids, context=context):
                    processed_ids.extend(self._action_explode(cr, uid, new_move, context=context))
            
            if not move.split_from and move.procurement_id:
                # Check if procurements have been made to wait for
                moves = move.procurement_id.move_ids
                if len(moves) == 1:
                    proc_obj.write(cr, uid, [move.procurement_id.id], {'state': 'done'}, context=context)

            if processed_ids and move.state == 'assigned':
                # Set the state of resulting moves according to 'assigned' as the original move is assigned
                move_obj.write(cr, uid, list(set(processed_ids) - set([move.id])), {'state': 'assigned'}, context=context)
                
            #Custom Code Start
            if not move.picking_id.sale_id and move.picking_id.picking_type_id.code == 'outgoing' \
               and bom_point.type == 'phantom':
                stock_picking_type_id = self.pool.get('stock.picking.type').search(cr,uid,[('code','=','incoming'),
                                                                                           ('warehouse_id','=',move.picking_id.picking_type_id.warehouse_id.id)],limit = 1)
                stock_picking_type_brw_rec = self.pool.get('stock.picking.type').browse(cr,uid,stock_picking_type_id)
                for entradas_line in bom_point.entradas_bom_line_ids:
                    move_vals = {'product_uos_qty': 0.0,
                                 'product_uom': entradas_line.product_uom.id,
                                 'price_unit': 0.0,
                                 'product_uom_qty': entradas_line.product_qty * move.product_uom_qty,
                                 'location_id': stock_picking_type_brw_rec.default_location_src_id.id,
                                 'location_dest_id': stock_picking_type_brw_rec.default_location_dest_id.id,
                                 'state': 'draft',
                                 'product_id': entradas_line.product_id.id,
                                 'name': entradas_line.product_id.product_tmpl_id.name}
                    new_move_id = move_obj.create(cr,uid,move_vals)
                    move_obj.action_done(cr,uid,new_move_id)
            #Custom Code End

            #delete the move with original product which is not relevant anymore
            move_obj.unlink(cr, SUPERUSER_ID, [move.id], context=context)
            #return list of newly created move
            return processed_ids
        return [move.id]

class stock_transfer_details(models.TransientModel):
    _inherit = 'stock.transfer_details'
    
    @api.one
    def do_detailed_transfer(self):
        if self.picking_id.state not in ['assigned', 'partially_available']:
            raise Warning(_('You cannot transfer a picking in state \'%s\'.') % self.picking_id.state)
        
        processed_ids = []
        # Create new and update existing pack operations
        for lstits in [self.item_ids, self.packop_ids]:
            for prod in lstits:
                pack_datas = {
                    'product_id': prod.product_id.id,
                    'product_uom_id': prod.product_uom_id.id,
                    'product_qty': prod.quantity,
                    'package_id': prod.package_id.id,
                    'lot_id': prod.lot_id.id,
                    'location_id': prod.sourceloc_id.id,
                    'location_dest_id': prod.destinationloc_id.id,
                    'result_package_id': prod.result_package_id.id,
                    'date': prod.date if prod.date else datetime.now(),
                    'owner_id': prod.owner_id.id,
                }
                if prod.packop_id:
                    prod.packop_id.with_context(no_recompute=True).write(pack_datas)
                    processed_ids.append(prod.packop_id.id)
                else:
                    pack_datas['picking_id'] = self.picking_id.id
                    packop_id = self.env['stock.pack.operation'].create(pack_datas)
                    processed_ids.append(packop_id.id)
        # Delete the others
        packops = self.env['stock.pack.operation'].search(['&', ('picking_id', '=', self.picking_id.id), '!', ('id', 'in', processed_ids)])
        packops.unlink()
        
        #Custome Code Start
        move_vals = {}
        sale_id = False
        if self.picking_id.sale_id:
            sale_id = self.picking_id.sale_id
        else:
            sale_id = self.env['sale.order'].search([('name', '=', self.picking_id.origin)], limit=1)
        if sale_id:
             default_data = self.env['purchase.order'].default_get(["picking_type_id"])
             picking_type_id = self.env['stock.picking.type'].browse(default_data.get('picking_type_id'))
             for line in sale_id.order_line:
                 bom_id = self.env['mrp.bom'].search([('product_tmpl_id', '=', line.product_id.product_tmpl_id.id),
                                                      ('type', '=', 'phantom')])
                 if bom_id:
                     for entradas_line in bom_id.entradas_bom_line_ids:
                         move_vals = {'product_uos_qty': 0.0,
                                      'product_uom': entradas_line.product_uom.id,
                                      'price_unit': 0.0,
                                      'product_uom_qty': entradas_line.product_qty * line.product_uom_qty,
                                      'location_id':picking_type_id.default_location_src_id.id,
                                      'location_dest_id':picking_type_id.default_location_dest_id.id,
                                      'state': 'draft',
                                      'product_id':entradas_line.product_id.id,
                                      'name':entradas_line.product_id.product_tmpl_id.name}
                         move_id = self.env['stock.move'].create(move_vals)
                         move_id.action_done()
        # Custome Code End

        # Execute the transfer of the picking
        self.picking_id.do_transfer()
        return True

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4
