# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2012-TODAY Acespritech Solutions Pvt Ltd <http://www.acespritech.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api, _
from openerp.addons.product import _common

class mrp_bom(models.Model):
    _inherit = 'mrp.bom'

    entradas_bom_line_ids = fields.One2many('entradas.bom.line', 'bom_id', 'Entradas BoM Lines', copy=True)

class entradas_bom_line(models.Model):
    _name = 'entradas.bom.line'
    _order = "sequence"
    _rec_name = "product_id"

    @api.model
    def _get_uom_id(self):
        return self.env['product.uom'].search([], limit=1, order='id')[0] 

    type = fields.Selection([('normal', 'Normal'), ('phantom', 'Phantom')], default='normal', string='BoM Line Type', required=True,)
    product_id = fields.Many2one('product.product', 'Product', required=True)
    product_uos_qty = fields.Float('Product UOS Qty')
    product_uos = fields.Many2one('product.uom', 'Product UOS')
    product_qty = fields.Float('Product Quantity', required=True, default=1)
    product_uom = fields.Many2one('product.uom', 'Product Unit of Measure', required=True, default=_get_uom_id)
    date_start = fields.Date('Valid From')
    date_stop = fields.Date('Valid Until')
    sequence = fields.Integer('Sequence', default=1)
    routing_id = fields.Many2one('mrp.routing', 'Routing')
    product_rounding = fields.Float('Product Rounding')
    product_efficiency = fields.Float('Manufacturing Efficiency', required=True, default=1)
    property_ids = fields.Many2many('mrp.property', string='Properties')
    bom_id = fields.Many2one('mrp.bom', 'Parent BoM', ondelete='cascade', select=True, required=True)
    attribute_value_ids = fields.Many2many('product.attribute.value', string='Variants')
#     child_line_ids = fields.One2many('entradas.bom.line',compute = _get_child_bom_lines,string="BOM lines of the referred bom")

    @api.onchange('product_uom')
    def onchange_uom(self):
        res = {'value': {}}
        if not self.product_uom or not self.product_id:
            return res
        if self.product_uom.category_id.id != self.product_id.uom_id.category_id.id:
            res['warning'] = {'title': _('Warning'), 'message': _('The Product Unit of Measure you chose has a different category than in the product form.')}
            res['value'].update({'product_uom': product.uom_id.id})
        return res

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = {}
        if self.product_id:
            res['value'] = {
                'product_uom': self.product_id.uom_id.id,
                'product_uos_qty': 0,
                'product_uos': False
            }
            if self.product_id.uos_id:
                res['value']['product_uos_qty'] = self.product_qty * self.product_id.uos_coeff
                res['value']['product_uos'] = self.product_id.uos_id.id
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4
